<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        // $admin_id=$this->session->userdata('admin_id');
        // if($admin_id==NULL){
        //     redirect('/admin-login');
        // }        
        
    }

	public function index()
	{   
		$this->load->view('admin/login/login');
    }
        

    public function admin_login_check(){

        $email =$this->input->post('email',TRUE);
        $password=$this->input->post('password',TRUE);

        $this->load->model('admin_model');

        $query=$this->admin_model->admin_login_info($email,$password);       
        $sdata=array();
		if ($query) {
                    $sdata['user_id']=$query->user_id;
                    $sdata['username']=$query->username;                     
                    $testarray =$this->session->set_userdata($sdata);
                    redirect('/dashboard');
                    //if not available the redirect with error message

		}else{
			$sdata['error_message']='Incorrect Email or Password';
			$this->session->set_userdata($sdata);			
			$this->load->view('admin/login/login',$sdata);

		}
    }
    //dashboard
    public function dashboard(){
        $data=array();
        $data['sidebar_menu']=$this->load->view('admin/partials/sidebar_menu.php','',TRUE);
        $data['main_content']=$this->load->view('admin/partials/main_content.php','',TRUE);
        $this->load->view('admin/master',$data);
    }

    //sign out
    public function sign_out(){
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
        $data=array();
        $data['error_message']="You have Successfully Logout";
        $this->session->set_userdata($data);
        redirect('/admin-login');
    }
    //Add portfolio
    public function add_portfolio(){
        $data=array();        
        $data['all_active_client_info']=$this->admin_model->all_active_client_info(); 
        $data['all_published_category']=$this->admin_model->all_published_category(); 
        $data['sidebar_menu']=$this->load->view('admin/partials/sidebar_menu.php','',TRUE);
        $data['main_content']=$this->load->view('admin/partials/add_portfolio.php',$data,TRUE);
        $this->load->view('admin/master',$data);
    }
    //Manage Portfolio

    public function manage_portfolio(){
        $data=array();
        $data['sidebar_menu']=$this->load->view('admin/partials/sidebar_menu.php','',TRUE);
        $data['main_content']=$this->load->view('admin/partials/manage_portfolio.php','',TRUE);
        $this->load->view('admin/master',$data); 
    }
    //category section
    public function add_category(){ 
        $data=array();              
        $data['sidebar_menu']=$this->load->view('admin/partials/sidebar_menu.php','',TRUE);
        $data['main_content']=$this->load->view('admin/partials/add_category.php','',TRUE);
        $this->load->view('admin/master',$data);
    }
    public function save_category(){
        $sdata=array();        
        $this->admin_model->save_category_info();        
        $sdata['message']="Category Inserted Successfully";
        redirect('add-category',$sdata);
    }
    public function manage_category(){        
        $data=array();        
        $data['all_published_categroy']=$this->admin_model->all_published_categroy();       
        $data['sidebar_menu']=$this->load->view('admin/partials/sidebar_menu.php','',TRUE);
        $data['main_content']=$this->load->view('admin/partials/manage_category.php',$data,TRUE);
        $this->load->view('admin/master',$data);
    }

    //Client section
    public function add_client(){
        $data=array();
        $data['sidebar_menu']=$this->load->view('admin/partials/sidebar_menu.php','',TRUE);
        $data['main_content']=$this->load->view('admin/partials/add_client.php','',TRUE);
        $this->load->view('admin/master',$data);
    }
    //save client
    public function save_client(){
        $sdata=array();        
        $this->admin_model->save_client_info();        
        $sdata['message']="Client Info Inserted Successfully";
        redirect('add-client',$sdata);
    }   

    //Manage clients
    public function manage_clients(){
        $data=array();        
        $data['all_client_info']=$this->admin_model->all_client_info(); 
        $data['sidebar_menu']=$this->load->view('admin/partials/sidebar_menu.php','',TRUE);
        $data['main_content']=$this->load->view('admin/partials/manage_clients.php',$data,TRUE);
        $this->load->view('admin/master',$data);
    }



}
